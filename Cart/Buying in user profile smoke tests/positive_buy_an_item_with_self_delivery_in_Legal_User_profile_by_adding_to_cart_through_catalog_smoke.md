#Корзина
# Оформление заказа с самовывозом в профиле юридического лица через выбор товара из каталога и добавление его в корзину

#Smoke


Cart11

Оформление заказа с самовывозом в профиле юридического лица через выбор товара из каталога и добавление его в корзину

* Тестовые данные:
  
  юр лицо: [petrovtest336@gmail.com](mailto:petrovtest336@gmail.com) пароль: 123ABCabc!
  
  Предусловие:
1. Открыта главная страница прода [https://stroyrem-nn.ru/](https://stroyrem-nn.ru/) или теста [https://test2.stroyrem-nn.ru/](https://test2.stroyrem-nn.ru/)
2. Тестовый юзер авторизован и открыт его профиль

* Шаги:
1. На главной странице нажать кнопку "Каталог товаров" (десктоп) / нажать на бургер-меню и выбрать "Каталог товаров" (тач)

2. Перейти в каталог и выбрать любой товар

3. Нажать кнопку "В корзину" на одном из товаров

4. Нажать на иконку "Корзина"

5. На странице Корзины нажать "Перейти к оформлению"

6. Выбрать вкладку "Юридическое лицо" и нажать "Продолжить"

7. В открывшемся окне выбрать вкладку "Самовывоз"

8. Выбрать дату Самовывоза

9. Отметить чекбокс "Согласен на обработку персональных данных"

10. Нажать кнопку "Оформить заказ"

* Ожидаемый результат:
  Оформление заказа проходит успешно, появляется модальное окно успеха, что заказ оформлен и данные заказа. На почту приходит письмо со всеми подробностями и ссылкой для оплаты

Автор: Татьяна Жукаускене

Отчет о тестировании

Тестовый сервер

| Дата       | Время | Версия браузера Десктоп              | Результат/Баг в Трелло Десктоп | Версия браузера и ОС Тач         | Результат/Баг в Трелло Тач | Дата релиза | QA      |
| ---------- | ----- | ----------| ---------| ------- | ------ | ----------- | ------- |
| 2023-09-25 | 13:00 | Chrome 117.0.5938.89 Firefox 117.0.1 | BLOCKED                        | Chrome 117.0.5938.60, Android 10 | BLOCKED                    | 17.09.2023  | Татьяна |
|2023-09-28|13:05|Chrome 117.0.5938.132 Firefox 118.0|PASS|Chrome 117.0.5938.60, Android 10|PASS|17.09.2023|Татьяна|

Продовый сервер

| Дата       | Время | Версия браузера Десктоп              | Результат/Баг в Трелло Десктоп     | Версия браузера и ОС Тач         | Результат/Баг в Трелло Тач         | Дата релиза | QA      |
| ---------- | ----- | ----------| ---------| ------- | ------ | ----------- | ------- |
| 2023-09-25 | 13:01 | Chrome 117.0.5938.89 Firefox 117.0.1 | FAIL https://trello.com/c/0VssmyaG | Chrome 117.0.5938.60, Android 10 | FAIL https://trello.com/c/0VssmyaG | 17.09.2023  | Татьяна |
|2023-10-01 | 09:26| Chrome  116.0.5845.190  | FAIL https://trello.com/c/0VssmyaG |   MIUI 14.0.2 Chrome | FAIL https://trello.com/c/0VssmyaG | 01.10.2023  | Алёна |
|            |       |                                      |                                    |                                  |                                    |             |         |
