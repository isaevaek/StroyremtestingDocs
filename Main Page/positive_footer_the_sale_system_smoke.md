Переход по ссылке "Система скидок" из футера.
	
* Предусловие:

 1.Открыта главная страница сайта:
 
 тест сервер - https://test2.stroyrem-nn.ru/
 
 прод сервер - https://stroyrem-nn.ru/
 
 2.Пользователь не авторизован
  
* Шаги:

  1. Прокрутить главную страницу до футера
  
  2. Нажать на ссылку "Система скидок"

* Ожидаемый результат:

   Открывается страница "Система скидок" https://stroyrem-nn.ru/nakopitelnaya-sistema-skidok с описанием, как получить клубную карту и бонусах ее владельцам.

Автор: Сабина

Тест выполнен на тестовом сервере

| Дата       | Время | Версия браузера Десктоп                  | Результат/Баг Трелло Десктоп | Версия браузера и ОС тача | Результат/Баг Трелло Тач | Дата релиза | Имя    |
|------------|-------|------------------------------------------|------------------------------|---------------------------|--------------------------|-------------|--------|
| 2023-07-19 | 08:20 | Chrome 114.0.5735.199 Yandex 23.7.0.2526 | PASS                         |                           |                          | 16.06.23    | Сабина |
|            |       |                                          |                              |                           |                          |             |        |

Тест выполнен на продовом сервере

| Дата       | Время | Версия браузера Десктоп                               | Результат/Баг Трелло Десктоп | Версия браузера и ОС тача                | Результат/Баг Трелло Тач | Дата релиза | Имя     |
|------------|-------|-------------------------------------------------------|------------------------------|------------------------------------------|--------------------------|-------------|---------|
| 2023-07-19 | 08:25 | Chrome 114.0.5735.199 Yandex 23.7.0.2526              | PASS                         |                                          |                          | 16.06.23    | Сабина  |
| 2023-08-14 | 13:45 | Chrome 115.0.5790.171 Firefox 115.0.3                 | PASS                         | Chrome 115.0.5790.166, Android 10        | PASS                     | 13.08.23    | Татьяна |
| 2023-08-30 | 15:45 | Chrome 115.0.5790.171                                 | PASSED                       | Chrome 115.0.5790.166, Android 13        | PASSED                   | 30.08.23    | Валерий |
| 2023-10-01 | 10:03 | Chrome версия 117.0.5938.132 Win 10 (x64) версия 22H2 | PASS                         | Chrome версия 117.0.5938.140 MIUI 12.1.2 | PASS                     | 2023-10-01  | NataM   |