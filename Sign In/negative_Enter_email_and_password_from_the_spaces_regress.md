Ввод в поля логин и пароль пробелов.

* Тестовые данные:


* Предварительные шаги:
1. Открыть форму авторизации на сайте Стройрем

* Шаги:
1. Ввести 10 пробелов в поле электронной почты
2. Ввести 10 пробелов в поле пароля
3. Нажать кнопку "Войти"

* Ожидаемый результат:
1. Выводится сообщение у поля электронной почты "Введите корректную электронную почту"


* Постусловие:

Автор: Александр Воронин

* Тестовый сервер

 
|  №  | Дата       | Время |           Версия браузера/Десктоп          |        Результат/Баг в Трелло Десктоп    |             Версия браузера и ОС Тач      |           Результат/Баг в Трелло Тач          |  Дата Релиза  |  Имя   |
| --- | ---------- | ----- |-------------------------------------| ---------------------------------- | ---------------------------------- | ---------------------------------- | ------| ------  |
| 1   | 2023-06-10 | 16:22 |Chrome 116.0.5845.97 Mazila 116.0.2  | Fail  https://trello.com/c/c1837dE2| Chrome 116.0.5845.97               | Fail https://trello.com/c/c1837dE2 | 04.07 | Александр Воронин  |
| 2   | 2023-08-12 | 20:01 |Chrome 116.0.5845.97 Yandex 23.7.2.765| Fail https://trello.com/c/c1837dE2| Chrome 116.0.5845.97               | Fail https://trello.com/c/c1837dE2 | 13.08 | Сабина  |
|I fucking what's the release number?|2023-09-29 | 13:54 | Opera One(версия: 102.0.4880.70) | Fail https://trello.com/c/Kgtlx2UC  |  |   | | Валерий|
| 4 |2023-10-05 | 22:34 | Chrome 117.0.5938.89 | Fail https://trello.com/c/c1837dE2 | Safari 16.6.1 | Fail https://trello.com/c/c1837dE2 | --- | Мария | 

* Продовый сервер


|  №  | Дата       | Время |           Версия браузера/Десктоп          |        Результат/Баг в Трелло Десктоп    |             Версия браузера и ОС Тач      |           Результат/Баг в Трелло Тач          |  Дата Релиза  |  Имя   |
| --- | ---------- | ----- |-------------------------------------| ---------------------------------- | ---------------------------------- | ---------------------------------- | ------| ------  |
| 1   | 2023-06-10 | 16:33 |Chrome 116.0.5845.97 Mazila 116.0.2  | Fail  https://trello.com/c/c1837dE2| Chrome 116.0.5845.97               | Fail https://trello.com/c/c1837dE2 | 04.07 | Александр Воронин  |
| 2   | 2023-08-13 | 19:39 |Chrome 116.0.5845.97 Yandex 23.7.2.765| Fail https://trello.com/c/c1837dE2| Chrome 116.0.5845.97               | Fail https://trello.com/c/c1837dE2 | 13.08 | Сабина  |